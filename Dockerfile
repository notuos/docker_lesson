FROM debian:9 as build
RUN apt update && apt install -y wget gcc make libpcre++-dev zlib1g-dev
RUN wget http://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz &&  cd nginx-1.0.5  && ./configure --sbin-path=/usr/local/nginx/nginx --pid-path=/usr/local/nginx/nginx.pid  && make && make install
CMD ["/usr/local/nginx", "-g", "daemon off"]  

FROM debian:9
WORKDIR /usr/local/nginx
COPY --from=build /usr/local/nginx .
RUN echo "1337" > /usr/local/nginx/html/index.html  
CMD ["./nginx", "-g", "daemon off;"]



